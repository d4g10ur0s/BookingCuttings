import React, { useState, useEffect } from 'react';
import { useCallback , useMemo } from 'react';

import type {PropsWithChildren} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  TextInput,
  TouchableOpacity,
  View,
  Image,
  Modal,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {ServiceSection} from "./Services.tsx"
import {ServiceCell} from "./Services.tsx"
import {ServiceContainer} from "./Services.tsx"
import {EmployeeSelection,EmployeeCell} from "./ShowDay.tsx"
import {DeletionModal, ErrorModal , PickerModal} from "./AppPopUps.tsx"

function ErrorInput(props){
  return (
    <Text
      style={styles.usernameError}
    >
      {props.msg}
    </Text>
  );
}

function ServiceForm(props){

  const [service, setService] = useState({
                                name : "",
                                duration : 0,
                                cost : 0,
                                });
  // submit to server
  const toSubmit = () => {
    props.toSubmit(service);
  }
  // service name error
  const [nameError , setNameError] = useState(null);
  // service name check
  const serviceNameCheck = (name) => {
    if(name.length>3){
      var tservice = service;
      tservice.name = name;
      setService(tservice);
      setNameError(null);
    }else{
      setNameError(<ErrorInput msg = {"Name must be over 3 characters long."} />);
    }
  }
  const nameInput = <TextInput style={styles.infoInput} placeholder={"Name"} placeholderTextColor={"#000"} onChangeText={(text) => serviceNameCheck(text)}/>;
  // service cost error
  const [costError ,setCostError] = useState(null);
  // service cost check
  const serviceCostCheck = (cost) => {
    if(isNaN(cost)){
      setCostError(<ErrorInput msg = {"Cost must be a valid number."} />);
    }else{
      if(cost > 200 || cost < 5){
        setCostError(<ErrorInput msg = {"Cost must be between 5 and 200 eur."} />);
      }else{
        var tservice = service;
        tservice.cost = cost;
        setService(tservice);
        setCostError(null);
      }
    }
  }
  const costInput = <TextInput style={styles.infoInput} placeholder={"Cost"} placeholderTextColor={"#000"} onChangeText={(text) => serviceCostCheck(text)}/>;
  // service duration error
  const [durationError , setDurationError] = useState(null);
  // service duration check
  const serviceDurationCheck = (duration) => {
    if(isNaN(duration)){
      setDurationError(<ErrorInput msg = {"Duration must be a valid number."} />);
    }else{
      if(duration > 240 || duration < 5){
        setDurationError(<ErrorInput msg = {"Duration must be between 5 and 240 minutes."} />);
      }else{
        var tservice = service;
        tservice.duration = duration;
        setService(tservice);
        setDurationError(null);
      }
    }
  }
  const durationInput = <TextInput style={styles.infoInput} placeholder={"Duration"} placeholderTextColor={"#000"} onChangeText={(text) => serviceDurationCheck(text)}/>;

  return(
    <View
      style={styles.employeeForm}
    >
      <Text
        style={styles.employeeFormHeader}
      >
        {"New Service"}
      </Text>
      {nameInput}
      {nameError}
      {costInput}
      {costError}
      {durationInput}
      {durationError}
      <TouchableOpacity
        style={styles.submitButton}
        onPress={toSubmit}
      >
        <Text>
          {"Submit"}
        </Text>
      </TouchableOpacity>
    </View>
  )
}

function EmployeeForm(props){
  // the user that adds an employee
  const [employee , setEmployee] = useState(props.employee);
  // connection error
  const [connectionError , setConnectionError] = useState(null);
  // user info
  const [user, setUser] = useState({
    name : "",
    surname : "",
    email : "",
    type_of_user : "",
  });
  // errors
  const [nameError , setNameError] = useState(null);
  const [surnameError , setSurnameError] = useState(null);
  const [emailError , setEmailError] = useState(null);
  const [phoneError , setPhoneError] = useState(null);
  //submit info
  const storeEmployee = async () => {
    //prepei na kanw gen ena tyxaio password
    if(nameError==null && surnameError==null && emailError==null && phoneError==null && user.name.length > 0){
      //edw 8a mpei to fetch ke an ola pane kala tote 8a vgazw thn forma .
      data = await fetch('http://192.168.1.226:8080/storeEmployee', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({user : user, employee: employee.employee})
      })
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Network response was not ok');
        }
      })
      .then(data => {
        return data;
      })
      .catch(error => {
        console.error('There was a problem with the fetch operation:', error);
      });
    }
    if(data.state==0){
      //yparxei error , 8a to grapseis katw katw
      setConnectionError(<ErrorInput msg = {data.msg} />);
    }else{
      // delete form
      console.log(data)
      props.formSubmitted();
    }
  }
  // name check callback
  const nameCheck = (smallname) => {
    if(smallname.length>3){
      var tuser = user;
      tuser.name = smallname.replace(/\s/g, "");
      setUser(tuser);
      setNameError(null);
    }else{
      setNameError(<ErrorInput msg = {"Name must be over 3 characters long."} />);
    }
  }
  // surname check callback
  const surnameCheck = (surname) => {
    if(surname.length>3){
      setSurnameError(null);
      var tuser = user;
      tuser.surname = surname.replace(/\s/g, "");
      setUser(tuser);
    }else{
      setSurnameError(<ErrorInput msg = {"Surname must be over 3 characters long."} />);
    }
  }
  // email check
  const emailCheck = (email) => {
    const emailRegex: RegExp = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
    if (emailRegex.test(email)) {
      setEmailError(null);
      var tuser = user;
      tuser.email = email.replace(/\s/g, "");
      setUser(tuser);
    }else{setEmailError(<ErrorInput msg = {"You provided an invalid e-mail address."} />);}
  }
  // check phone callback
  const phoneCheck = (phone) => {
    const phoneRegex = /^\d{10}$/;
    if(phoneRegex.test(phone)){
      setPhoneError(null);
      var tuser = user;
      tuser.phone_num = phone.replace(/\s/g, "");
      setUser(tuser);
    }else{setPhoneError(<ErrorInput msg = {"Phone must be 10 digits long."} />);}
  }
  // a type of user button is selected
  const [selected , setSelected] = useState([false, false, false]);
  const button1 = () => {
    setSelected([true ,false ,false]);
    var tuser = user;
    tuser.type_of_user = "1";
    setUser(tuser);
  }
  const button2 = () => {
    setSelected([false ,true ,false]);
    var tuser = user;
    tuser.type_of_user = "2";
    setUser(tuser);
  }
  const button3 = () => {
    setSelected([false ,false ,true]);
    var tuser = user;
    tuser.type_of_user = "3";
    setUser(tuser);
  }
  // form inputs
  const nameInput = <TextInput style={styles.infoInput} placeholder={"Name"} placeholderTextColor={"#000"} onChangeText={(text) => nameCheck(text)}/>;
  const surnameInput = <TextInput style={styles.infoInput} placeholder={"Surname"} placeholderTextColor={"#000"} onChangeText={(text) => surnameCheck(text)}/>;
  const emailInput = <TextInput style={styles.infoInput} placeholder={"Email"} placeholderTextColor={"#000"} onChangeText={(text) => emailCheck(text)}/>;
  const phoneInput = <TextInput style={styles.infoInput} placeholder={"Phone Num."} placeholderTextColor={"#000"} onChangeText={(text) => phoneCheck(text)}/>;

  return(
    <View
      style={styles.employeeForm}
    >
      <Text
        style={styles.employeeFormHeader}
      >
        {"New Employee"}
      </Text>
      {nameInput}
      {nameError}
      {surnameInput}
      {surnameError}
      {emailInput}
      {emailError}
      {phoneInput}
      {phoneError}
      <View
        style={styles.typeOfUserView}
      >
      <TouchableOpacity
        style={selected[0] ? styles.submitButtonDisabled : styles.submitButton}
        disabled={selected[0]}
        onPress={() => button1()}
      >
        <Text>
          {"Type 1"}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={selected[1] ? styles.submitButtonDisabled : styles.submitButton}
        disabled={selected[1]}
        onPress={() => button2()}
      >
        <Text>
          {"Type 2"}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={selected[2] ? styles.submitButtonDisabled : styles.submitButton}
        disabled={selected[2]}
        onPress={() => button3()}
      >
        <Text>
          {"Type 3"}
        </Text>
      </TouchableOpacity>
      </View>
      <TouchableOpacity
        style={styles.submitButton}
        onPress={() => storeEmployee()}
      >
        <Text>
          {"Submit"}
        </Text>
      </TouchableOpacity>
      {connectionError}
    </View>
  )
}

export function EmployeeContainer(props){

  const [modalErrorVisible, setModalErrorVisible] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [employee, setEmployee] = useState(props.employee);
  const [num ,setNum] = useState(props.num)
  // submit deletion
  function submitDeletion() {
    setModalVisible(true)
  }
  // cancel error
  const cancelError = () => {
    setModalErrorVisible(false);
  }
  // cancel deletion
  const cancelDeletion = () => {
    setModalVisible(false);
  }
  // delete employee
  const deleteEmployee = () => {
    if(props.email===employee.email){
      //prepei na valw modal
      setModalVisible(false);
      setModalErrorVisible(true);
    }else{
      if(props.type_of_employee === '1'){
        fetch('http://192.168.1.226:8080/deleteEmployee', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({employee: employee})
        })
        .then(response => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error('Network response was not ok');
          }
        })
        .then(data => {
          if(data.state==0){
            //yparxei error , 8a to grapseis katw katw
            setConnectionError(<ErrorInput msg = {data.msg} />);
          }else{
            // delete form
            setEmployeeForm(null);
            setFormExistence(false);
          }
        })
        .catch(error => {
          console.error('There was a problem with the fetch operation:', error);
        });
      }else{
        //kati allo edw
      }
    }
  }

  return(
    <View
      style={styles.emploeeContainer}
    >
      <View
        style={styles.emploeeCell}
      >
        <View
          style={styles.headerContainer}
        >
          <Image
           source={{uri: 'https://reactjs.org/logo-og.png'}}
           style={{alignSelf : "center",width: 100, height: 100,borderRadius : 8,marginBottom : 5,marginTop : 5,}}
          />
          <Text
            style={styles.employeeHeader}
          >
            {props.employee.name+ " " + props.employee.surname}
          </Text>
        </View>
        <View
          style={styles.shopInfo}
        >
          <Text
            style={styles.shopInfoTextHeader}
          >
            {"Employee Info"}
          </Text>
          <Text
            style={styles.shopInfoText}
          >
            {"Services : "}
          </Text>
          <Text
            style={styles.shopInfoText}
          >
            {"Active Appointments : "}
          </Text>
          <Text
            style={styles.shopInfoTextHeader}
          >
            {"A note !"}
          </Text>
          <Text
            style={styles.shopInfoText}
          >
            {"The note \n ... \n ... \n ... \n..."}
          </Text>
        </View>
      </View>
      <ScrollView
        horizontal={true}
        style = {styles.buttonScrollView}
      >
        <TouchableOpacity
          style={styles.controlButtons}
          onPress={() => toCalendar()}
        >
          <Text>
            {"Employee Profile"}
            </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.controlButtons}
          onPress={() => toShop()}
        >
          <Text>
            {"Assign Service"}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={(props.type_of_employee === '1') ? styles.deleteButton:styles.deleteButtonDisabled}
          disabled={!(props.type_of_employee === '1')}
          onPress={submitDeletion}
        >
          <Text>
            {"Delete Employee"}
          </Text>
        </TouchableOpacity>
      </ScrollView>
      <ErrorModal visible={modalErrorVisible} isVisible={cancelError}/>
      <DeletionModal visible={modalVisible} isVisible={cancelDeletion} employeeDeletion={deleteEmployee}/>
    </View>
  )
}

function EmployeeSection(props){

  const [content , setContent] = useState(props.content);
  return (
    <View
      style={styles.employeeSectionView}
    >
      {content}
    </View>
  );

}

function AddForm(props): JSX.Element {

  const pressCallback = () => {
    props.toForm()
  }

  return (
    <TouchableOpacity
      style={styles.controlButtons}
      onPress={props.toForm}
    >
      <Text>
        {props.text}
      </Text>
    </TouchableOpacity>
  );

}

export function ShopSection(props): JSX.Element {

  const [employee , setEmployee] = useState(props.employee)
  const [content , setContent] = useState(null);
  const [employeeForm , setEmployeeForm] = useState(null);
  const [shopname , setShopname] = useState(props.employee.employee.shop_name);
  const [formExistence, setFormExistence] = useState(false);
  // retrieve employees from database
  const retrieveEmployees = async () =>{
    var empl = await fetch('http://192.168.1.226:8080/retrieveEmployees', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({user : employee.user, employee: employee.employee})
    })
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Network response was not ok');
      }
    })
    .then(data => {
      if(data.state==0){
        //yparxei error , 8a to grapseis katw katw
        setConnectionError(<ErrorInput msg = {data.msg} />);
      }else{
        var empl = []
        // services are retrieved
        //create UI
        for(i in data.employees){
          empl.push(data.employees[i]);
        }
        return empl
      }
    })
    .catch(error => {
      console.error('There was a problem with the fetch operation:', error);
    });
    return empl;
  }
  const onLoad = async () => {
    // get employee's
    var employees = await retrieveEmployees();
    emp_containers = [];
    for(i in employees){
      emp_containers.push(<EmployeeContainer email={employee.user.email} employee={employees[i]} num={i} type_of_employee = {employee.employee.type_of_employee}/>);
    }
    setContent(<EmployeeSection content={emp_containers}/>);
  }
  // retrieve employees from database
  useEffect(() => {
    if(content == null){
      console.log("einai null");
      onLoad();
    }else{
      console.log("gemise")
    }
  }, [content]);

  const formSubmitted = () => {
    setEmployeeForm(null);
    setFormExistence(false);
  }
  const toEmployeeForm = () => {
    console.log("edw");
    if(!formExistence){
      setEmployeeForm(<EmployeeForm employee={props.employee} formSubmitted={formSubmitted}/>);
    }else{
      setEmployeeForm(null);
    }
    setFormExistence(!formExistence);
  }
  const [addForm, setAddForm] = useState(<AddForm text={"Add Employee"} toForm={toEmployeeForm}/>);

  const toShop = () => {
    /*
    // Approach 1
    var b = await onLoad();
    setContent(b);
    */
    // Approach 2
    setContent(null);
    setAddForm(<AddForm text={"Add Employee"} toForm={toEmployeeForm}/>);
    setEmployeeForm(null);
    setFormExistence(false);
  }
  // connection error is an error for both the network and the service existence
  const [connectionError ,setConnectionError] = useState(null);
  const [services , setServices] = useState(null);
  //save service to database
  const saveService = (service) => {
    fetch('http://192.168.1.226:8080/saveService', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({user : employee.user, employee: employee.employee , service : service})
    })
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Network response was not ok');
      }
    })
    .then(data => {
      if(data.state==0){
        //yparxei error , 8a to grapseis katw katw
        setConnectionError(<ErrorInput msg = {data.msg} />);
      }else{
        // delete form
        setEmployeeForm(null);
        setFormExistence(false);
      }
    })
    .catch(error => {
      console.error('There was a problem with the fetch operation:', error);
    });
  }
  // reveal and unreveal the service form
  function toServiceForm(){
    if(!formExistence){
      setEmployeeForm(<ServiceForm toSubmit={saveService}/>);
    }else{
      setEmployeeForm(null);
    }
    setFormExistence(!formExistence);
  }
  // create services
  const createServices = async (services) => {
    //1. Create UI for each service
    var ret = [];
    for(i in services){
      ret.push(<ServiceContainer user={props.employee} service={services[i]}/>)
    }
    return ret;
  }
  // create services
  // retrieve services
  const retrieveServices = async () => {
    var serv = await fetch('http://192.168.1.226:8080/retrieveServices', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({user : employee.user, employee: employee.employee})
    })
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Network response was not ok');
      }
    })
    .then(data => {
      if(data.state==0){
        //yparxei error , 8a to grapseis katw katw
        setConnectionError(<ErrorInput msg = {data.msg} />);
      }else{
        var serv = []
        // services are retrieved
        //create UI
        for(i in data.services){
          serv.push(data.services[i]);
        }
        return serv
      }
    })
    .catch(error => {
      console.error('There was a problem with the fetch operation:', error);
    });
    return serv;
  }
  // retrieve services
  const toServices = async () => {//8elw ke connection gia na parw services
    var serviceSectionContent = await createServices(await retrieveServices());
    setContent(<ServiceSection employee={employee} services={serviceSectionContent}/>);
    setAddForm(<AddForm text={"Add Service"} toForm={toServiceForm}/>);
    setFormExistence(false);
    setEmployeeForm(null);
  }

  function toClients(){
    setContent(null);
  }

  return (
    <View
      style={styles.shopView}
    >
      <Text
        style={styles.shopHeader}
      >
        {shopname}
      </Text>
      <ScrollView
        horizontal={true}
        style = {styles.buttonScrollView}
      >
        {addForm}
        {connectionError}
        <TouchableOpacity
          style={styles.controlButtons}
          onPress={() => toShop()}
        >
          <Text>
            {"Show Employees"}
            </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.controlButtons}
          onPress={() => toServices()}
        >
          <Text>
            {"Show Services"}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.controlButtons}
          onPress={() => toClients()}
        >
          <Text>
            {"Show Clients"}
          </Text>
        </TouchableOpacity>
      </ScrollView>
      {employeeForm}
      {content}
    </View>
    );
}

const styles = StyleSheet.create({
  shopView : {
    backgroundColor : '#322c47c5',
    borderRadius : 5,
    padding : 15,
    margin : 10,
  },
  shopHeader : {
    alignSelf : "center",
    margin : 5,
    fontSize : 35,
    textAlign : "center",
    color : "#FFFFFFFF",
    fontWeight : "700",
  },
  emploeeContainer : {
    flexDirection : "column",
    borderRadius : 8,
    backgroundColor : "#FFFFFF59",
    marginLeft : 5,
    marginRight : 5,
    marginTop : 5,
    marginBottom : 10,
  },
  emploeeCell : {
    flexDirection : "row",
    borderRadius : 8,
    marginLeft : 5,
    marginRight : 5,
    marginTop : 5,
  },
  headerContainer : {
    flex : 0,
    alignItems : "center",
    width : "45%",
    margin: 2,
  },
  employeeHeader : {
    fontSize : 17,
    textAlign : "center",
    fontWeight : "500",
    textAlign : "left",
    paddingLeft : 10,
    paddingTop : 3,
    paddingBottom : 3,
    margin : 3,
  },
  shopInfo : {
    flex:1,
    alignItems : "center",
  },
  shopInfoTextHeader : {
    textAlign : "center",
    margin : 3,
    color : "#000000FF",
    fontSize : 13,
    fontWeight : 700,
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    borderBottomRightRadius: 5,
    borderBottomLeftRadius: 5,
    paddingLeft : 10,
    paddingRight : 10,
  },
  shopInfoText : {
    textAlign : "center",
    margin : 3,
    color : "#000000FF",
    fontSize : 10,
    fontWeight : 700,
  },
  buttonScrollView : {
    width : "100%",
    backgroundColor : "#FFFFFF59",
    paddingLeft : 7,
    paddingRight : 7,
    paddingTop : 2,
    paddingBottom : 2,
    marginTop : 10,
    marginBottom : 2,
    alignSelf : "center",
    borderRadius : 8,
  },
  controlButtons : {
    fontSize : 12,
    textAlign : "center",
    marginLeft : 8,
    marginTop : 2,
    marginBottom : 2,
    marginRight : 8,
    padding : 8,
    borderRadius : 8,
    backgroundColor : "#ebd2817b",
  },
  employeeForm : {
    alignItems : "center",
    borderRadius : 8,
    backgroundColor : "#FFFFFF59",
    marginLeft : 5,
    marginRight : 5,
    marginTop : 5,
    marginBottom : 10,
    paddingBottom : 5,
  },
  employeeFormHeader : {
    fontSize : 20,
    textAlign : "center",
    fontWeight : "500",
    textAlign : "left",
    paddingLeft : 10,
    paddingTop : 3,
    paddingBottom : 3,
    margin : 3,
  },
  infoInput : {
    width : 225,
    fontSize : 15,
    color : "#000000FF",
    backgroundColor : "#FFFFFF59",
    paddingTop : 5,
    paddingBottom : 5,
    paddingRight : 5,
    paddingLeft : 10,
    margin : 2,
    borderRadius : 8,
  },
  submitButton : {
    fontSize : 10,
    textAlign : "center",
    marginLeft : 8,
    marginTop : 2,
    marginBottom : 2,
    marginRight : 8,
    padding : 8,
    borderRadius : 8,
    backgroundColor : "#554488FF",
  },
  submitButtonDisabled : {
    fontSize : 10,
    textAlign : "center",
    marginLeft : 8,
    marginTop : 2,
    marginBottom : 2,
    marginRight : 8,
    padding : 8,
    borderRadius : 8,
    backgroundColor : "#55448877",
  },
  usernameError : {
    fontSize : 10,
    textAlign : "left",
    marginTop : 1,
    marginBottom : 5,
    paddingLeft : 25,
    color : "#FF5555FF",
  },
  typeOfUserView : {
    flexDirection : "row",
    alignItems : "center",
    borderRadius : 8,
    backgroundColor : "#FFFFFF59",
    margin : 5,
    padding : 3,
  },
  deleteButton : {
    "fontSize" : 12,
    "textAlign" : "center",
    "marginLeft" : 8,
    "marginTop" : 2,
    "marginBottom" : 2,
    "marginRight" : 8,
    "padding" : 8,
    "borderRadius" : 8,
    "backgroundColor" : "#FF4455FF",
  },
  deleteButtonDisabled : {
    "fontSize" : 12,
    "textAlign" : "center",
    "marginLeft" : 8,
    "marginTop" : 2,
    "marginBottom" : 2,
    "marginRight" : 8,
    "padding" : 8,
    "borderRadius" : 8,
    "backgroundColor" : "#FF4455ac",
  },
  modalView : {
    marginTop : "60%",
    marginLeft :"15%",
    width : 250,
    backgroundColor : "#FFFFFFdc",
    borderRadius : 8,
    textAlign : "center",
    alignItems:"center",
    justifyContent: "center",
  },
  modalHeader : {
    color:"#000000FF",
    padding : 5,
    borderBottomColor : "#000000FF",
    borderBottomWidth:2,
    fontSize:12,
    width : "100%",
    alignSelf:"center",
    textAlign:"center",
  },
});
