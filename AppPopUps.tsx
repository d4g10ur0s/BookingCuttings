// import statements
import React, { useState, useEffect } from 'react';
import { useCallback , useMemo } from 'react';
import type {PropsWithChildren} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  TextInput,
  TouchableOpacity,
  View,
  Image,
  Modal,
} from 'react-native';
import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {ServiceSection} from "./Services.tsx"
import {ServiceCell} from "./Services.tsx"
import {ServiceContainer} from "./Services.tsx"
import {EmployeeCell,EmployeeSelection} from "./AppSelections.tsx"
// import statements
// picker modal
export function PickerModal(props) {
  // dismiss modal
  function dismissPickerModal() {
    props.isVisible()
  }
  // UI
  return (
    <Modal animationType="slide" transparent={true} visible={props.visible} style={{alignSelf : "center",alignItems : "center",}}>
      <View
        style={[styles.modalView, {width : 270,marginLeft : "13%",}]}
      >
        <Text
        style={styles.modalHeader}
        >
          {"Service Assignment : "+props.serviceName}
        </Text>
        <View
          style={{height : 200,}}
        >
          <Text
          style={[styles.modalHeader,{fontSize : 15,width : null,borderBottomWidth:0,margin : 5,}]}
          >
            {"Pick an Employee for "+props.serviceName+"."}
          </Text>
          <EmployeeSelection user={props.user}/>
        </View>
        <View
          style={{flexDirection : "row",alignSelf : "center",}}
        >
          <TouchableOpacity
          onPress={dismissPickerModal}
          style={[styles.submitButton,{textAlign : "center",alignSelf:"center",}]}
          >
            <Text>
              {"Submit"}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
          onPress={dismissPickerModal}
          style={[styles.submitButton,{textAlign : "center",alignSelf:"center",}]}
          >
            <Text>
              {"Cancel"}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
    );
}
// picker modal
// error modal
export function ErrorModal(props) {
  const [error, setError] = useState(null);

  useEffect(() => {
    if (error) {
      console.error(error);
    }
  }, [error]);

  function showErrorModal(errorMessage) {
    setError(errorMessage);
  }

  function dismissErrorModal() {
    props.isVisible()
  }

  return (
    <Modal animationType="slide" transparent={true} visible={props.visible} style={{alignSelf : "center",alignItems : "center",}}>
      <View
        style={styles.modalView}
      >
        <Text
        style={styles.modalHeader}
        >
          {"Error Message"}
        </Text>
        <View>
          <Text
          style={[styles.modalHeader,{fontSize : 15,width : null,borderBottomWidth:0,margin : 5,}]}
          >
            {"You cannot delete yourself ."}
          </Text>
          <View
            style={{flexDirection : "row",alignSelf : "center",}}
          >
            <TouchableOpacity
             onPress={dismissErrorModal}
             style={[styles.submitButton,{textAlign : "center",alignSelf:"center",}]}
            >
              <Text>
                {"Cancel"}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
    );
}
// error modal
// deletion modal
export function DeletionModal(props) {
  const [error, setError] = useState(null);

  useEffect(() => {
    if (error) {
      console.error(error);
    }
  }, [error]);

  function showErrorModal(errorMessage) {
    setError(errorMessage);
  }

  function dismissErrorModal() {
    props.isVisible()
  }

  function  deleteEmployee() {
    props.employeeDeletion()
  }

  return (
    <Modal animationType="slide" transparent={true} visible={props.visible} style={{alignSelf : "center",alignItems : "center",}}>
      <View
        style={styles.modalView}
      >
        <Text
        style={styles.modalHeader}
        >
          {"Submit Message"}
        </Text>
        <View>
          <Text
          style={[styles.modalHeader,{fontSize : 15,width : null,borderBottomWidth:0,margin : 5,}]}
          >
            {"Are you sure you want to delete employee"}
          </Text>
          <View
            style={{flexDirection : "row",alignSelf : "center",}}
          >
            <TouchableOpacity
             onPress={deleteEmployee}
             style={[styles.submitButton,{textAlign : "center",alignSelf:"center",}]}
            >
              <Text>
                {"Submit"}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
             onPress={dismissErrorModal}
             style={[styles.submitButton,{textAlign : "center",alignSelf:"center",}]}
            >
              <Text>
                {"Cancel"}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
    );
}
// deletion modal

const styles = StyleSheet.create({
  submitButton : {
    fontSize : 10,
    textAlign : "center",
    marginLeft : 8,
    marginTop : 2,
    marginBottom : 2,
    marginRight : 8,
    padding : 8,
    borderRadius : 8,
    backgroundColor : "#554488FF",
  },
  modalView : {
    marginTop : "60%",
    marginLeft :"15%",
    width : 250,
    backgroundColor : "#FFFFFFdc",
    borderRadius : 8,
    textAlign : "center",
    alignItems:"center",
    justifyContent: "center",
  },
  modalHeader : {
    color:"#000000FF",
    padding : 5,
    borderBottomColor : "#000000FF",
    borderBottomWidth:2,
    fontSize:12,
    width : "100%",
    alignSelf:"center",
    textAlign:"center",
  },
});
