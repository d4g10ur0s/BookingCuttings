var createError = require('http-errors');
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var cors = require('cors')

var http = require('http');
var util = require('util');
var mysql = require('mysql');
const url = require('url');
var formidable = require('formidable');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, '/'));
app.set('view engine', 'ejs');
//to connection se db
const pool = mysql.createPool({//sundesh se vash
  connectionLimit : 10,
  host: "localhost",
  user: "root",
  password: "aledadu071",
  database: "BookingProject",
  multipleStatements: true
});
// assigned to service
app.all('/assignedToService',function (req, res) {
  console.log('Request received: ');
  util.inspect(req) // this line helps you inspect the request so you can see whether the data is in the url (GET) or the req body (POST)
  util.log('Request recieved: \nmethod: ' + req.method + '\nurl: ' + req.url) // this line logs just the method and url
  if(req.method==='OPTIONS'){
    res.writeHead(200);
    res.end();
  }else if(req.method==='POST'){
    var body = [];
    //h katallhlh kefalida
    res.writeHead(200, {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
    });
    //diavase data
    req.on("data", (chunk) => {
      console.log(chunk);
      body.push(chunk);
    });
    //otan exeis diavasei olo to data
    req.on("end", () => {
      var mdata = Buffer.concat(body).toString();
      mdata = JSON.parse(mdata);//parsing json
      console.log(mdata);
      pool.getConnection(async function(err, con) {
        console.log("Connected");
        const query = util.promisify(con.query).bind(con);//gia na exw promises
        var mquery = "SELECT * from employee_service where sid="+mdata.service.id+" ;";
        var result = await query(mquery);
        if (err){
          console.log(err);
        }
        if(result.length > 0){
          var emplIds= []
          for(i in result){emplIds.push(result[i].eid)}
          mquery = "SELECT * from employee where id IN "+`('${(emplIds).join("','")}')`
          +" as t1 INNER JOIN (SELECT * FROM user) as t2 ON t2.id="+"t1.id ;";//8elw ena inner join
          result = await query(mquery)
          res.write(JSON.stringify(result));
          res.end();
        }else{
          message = {state : 0, msg : "There are no services assigned to selected employee."};
          res.write(JSON.stringify(message));
          res.end();
        }
        con.release();
      });//telos connect
    });//req on end
    res.on('error', (err) => {
      console.error(err);
    });
  }//end if
});
// gia assigned to service
// assigned to employee
app.all('/assignedToEmployee',function (req, res) {
  console.log('Request received: ');
  util.inspect(req) // this line helps you inspect the request so you can see whether the data is in the url (GET) or the req body (POST)
  util.log('Request recieved: \nmethod: ' + req.method + '\nurl: ' + req.url) // this line logs just the method and url
  if(req.method==='OPTIONS'){
    res.writeHead(200);
    res.end();
  }else if(req.method==='POST'){
    var body = [];
    //h katallhlh kefalida
    res.writeHead(200, {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
    });
    //diavase data
    req.on("data", (chunk) => {
      console.log(chunk);
      body.push(chunk);
    });
    //otan exeis diavasei olo to data
    req.on("end", () => {
      var mdata = Buffer.concat(body).toString();
      mdata = JSON.parse(mdata);//parsing json
      console.log(mdata);
      pool.getConnection(async function(err, con) {
        console.log("Connected");
        const query = util.promisify(con.query).bind(con);//gia na exw promises
        var mquery = "SELECT * from employee_service where eid="+mdata.employee.id+" ;";
        var result = await query(mquery);
        if (err){
          console.log(err);
        }
        if(result.length > 0){
          var servIds= []
          for(i in result){servIds.push(result[i].sid)}
          mquery = "SELECT * from service where id IN "+`('${(servIds).join("','")}')`+" ;";
          result = await query(mquery)
          res.write(JSON.stringify(result));
          res.end();
        }else{
          message = {state : 0, msg : "There are no services assigned to selected employee."};
          res.write(JSON.stringify(message));
          res.end();
        }
        con.release();
      });//telos connect
    });//req on end
    res.on('error', (err) => {
      console.error(err);
    });
  }//end if
});
// gia assigned to employee
// assign services
app.all('/assignService',function (req, res) {
  console.log('Request received: ');
  util.inspect(req) // this line helps you inspect the request so you can see whether the data is in the url (GET) or the req body (POST)
  util.log('Request recieved: \nmethod: ' + req.method + '\nurl: ' + req.url) // this line logs just the method and url
  if(req.method==='OPTIONS'){
    res.writeHead(200);
    res.end();
  }else if(req.method==='POST'){
    var body = [];
    //h katallhlh kefalida
    res.writeHead(200, {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
    });
    //diavase data
    req.on("data", (chunk) => {
      console.log(chunk);
      body.push(chunk);
    });
    //otan exeis diavasei olo to data
    req.on("end", () => {
      var mdata = Buffer.concat(body).toString();
      mdata = JSON.parse(mdata);//parsing json
      console.log(mdata);
      pool.getConnection(async function(err, con) {
        console.log("Connected");
        const query = util.promisify(con.query).bind(con);//gia na exw promises
        var mquery = "INSERT INTO service(eid,sid) VALUES "+mdata.employee.id
                     +","+mdata.service.id+" ;";
        var result = await query(mquery);
        if (err){
          console.log(err);
        }
        message = {state : 0, msg : "Service is updated."};
        res.write(JSON.stringify(message));
        res.end();
        con.release();
      });//telos connect
    });//req on end
    res.on('error', (err) => {
      console.error(err);
    });
  }//end if
});
// gia assign services
// delete employee
app.all('/deleteEmployee',function (req, res) {
  console.log('Request received: ');
  util.inspect(req) // this line helps you inspect the request so you can see whether the data is in the url (GET) or the req body (POST)
  util.log('Request recieved: \nmethod: ' + req.method + '\nurl: ' + req.url) // this line logs just the method and url
  if(req.method==='OPTIONS'){
    res.writeHead(200);
    res.end();
  }else if(req.method==='POST'){
    var body = [];
    //h katallhlh kefalida
    res.writeHead(200, {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
    });
    //diavase data
    req.on("data", (chunk) => {
      console.log(chunk);
      body.push(chunk);
    });
    //otan exeis diavasei olo to data
    req.on("end", () => {
      var mdata = Buffer.concat(body).toString();
      mdata = JSON.parse(mdata);//parsing json
      console.log(mdata);
      pool.getConnection(async function(err, con) {
        console.log("Connected");
        const query = util.promisify(con.query).bind(con);//g9ia na exw promises
        var mquery = "DELETE FROM employee WHERE id="
                      +mdata.employee.id+";";
        query(mquery);
        mquery = "DELETE FROM employee_service WHERE eid="
                 +mdata.employee.id+";";
        query(mquery);
        if (err){
          console.log(err);
        }
        message = {state : 0, msg : "Employee has been deleted succesfully."};
        res.write(JSON.stringify(message));
        res.end();
        con.release();
      });//telos connect
    });//req on end

    res.on('error', (err) => {
      console.error(err);
    });
  }//end if
});
// gia delete employee
// retrieve employees
app.all('/retrieveEmployees',function (req, res) {
  console.log('Request received: ');
  util.inspect(req) // this line helps you inspect the request so you can see whether the data is in the url (GET) or the req body (POST)
  util.log('Request recieved: \nmethod: ' + req.method + '\nurl: ' + req.url) // this line logs just the method and url
  if(req.method==='OPTIONS'){
    res.writeHead(200);
    res.end();
  }else if(req.method==='POST'){
    var body = [];
    //h katallhlh kefalida
    res.writeHead(200, {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
    });
    //diavase data
    req.on("data", (chunk) => {
      console.log(chunk);
      body.push(chunk);
    });
    //otan exeis diavasei olo to data
    req.on("end", () => {
      var mdata = Buffer.concat(body).toString();
      mdata = JSON.parse(mdata);//parsing json
      console.log(mdata);
      pool.getConnection(async function(err, con) {
        console.log("Connected");
        const query = util.promisify(con.query).bind(con);//g9ia na exw promises
        var mquery = "SELECT * FROM employee WHERE shop_name like \'"
                      +mdata.employee.shop_name+"\' ;";
        var result = await query(mquery);
        var emplo = []
        for(i in result){
          emplo.push(result[i].id)
        }
        mquery = "SELECT * FROM user WHERE id IN "
                      +`('${(emplo).join("','")}')`+" ;";
        result = await query(mquery);
        if (err){
          console.log(err);
        }
        if(result.length == 0){// Store Service
          message = {state : 0, msg : "There are no employees stored yet. \nTry creating one ."};
          res.write(JSON.stringify(message));
          res.end();
        }else{
          console.log(result);
          message = {state : 1,employees : result ,msg : "Employees retrieved succesfully."};
          res.write(JSON.stringify(message));
          res.end();
        }
        con.release();
      });//telos connect
    });//req on end

    res.on('error', (err) => {
      console.error(err);
    });
  }//end if
});
// gia retrieve employees
// store employee
app.all('/storeEmployee',function (req, res) {
  console.log('Request received: ');
  util.inspect(req) // this line helps you inspect the request so you can see whether the data is in the url (GET) or the req body (POST)
  util.log('Request recieved: \nmethod: ' + req.method + '\nurl: ' + req.url) // this line logs just the method and url
  if(req.method==='OPTIONS'){
    res.writeHead(200);
    res.end();
  }else if(req.method==='POST'){
    var body = [];
    //h katallhlh kefalida
    res.writeHead(200, {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
    });
    //diavase data
    req.on("data", (chunk) => {
      console.log(chunk);
      body.push(chunk);
    });
    //otan exeis diavasei olo to data
    req.on("end", () => {
      var mdata = Buffer.concat(body).toString();
      mdata = JSON.parse(mdata);//parsing json
      console.log(mdata);
      pool.getConnection(async function(err, con) {
        console.log("Connected");
        const query = util.promisify(con.query).bind(con);//g9ia na exw promises
        var mquery = "SELECT * FROM user WHERE email like \'"
                      +mdata.user.email+"\' ;";
        var result = await query(mquery);
        if (err){
          console.log(err);
        }
        if(result.length == 0){// Store Service
          var mquery = "INSERT INTO user(name, surname , password, email , phone_num , type_of_user) values (\'"
                        +mdata.user.name+"\' ,\'"+mdata.user.surname+"\',\'"
                        +mdata.user.password+"\' ,\'"+mdata.user.email+"\',\'"
                        +mdata.user.phone_num+"\',\'employee\');";
          await query(mquery);
          var mquery = "SELECT id FROM user WHERE email like \'"
                        +mdata.user.email+"\' ;";
          result = await query(mquery);
          var mquery = "INSERT INTO employee(id,type_of_employee ,shop_name,shop_owner) values ("
                        +result[0].id+" ,\'"+mdata.user.type_of_user+"\',\'"
                        +mdata.employee.shop_name+"\' ,\'"+mdata.employee.shop_owner
                        +"\');";
          await query(mquery);
          message = {state : 1,employee : result ,msg : "Employee stored succesfully."};
          res.write(JSON.stringify(message));
          res.end();
        }else{
          message = {state : 0, msg : "There is an employee stored with this email."};
          res.write(JSON.stringify(message));
          res.end();
        }
        con.release();
      });//telos connect
    });//req on end

    res.on('error', (err) => {
      console.error(err);
    });
  }//end if
});
// gia store employee
// delete service
app.all('/deleteService',function (req, res) {
  console.log('Request received: ');
  util.inspect(req) // this line helps you inspect the request so you can see whether the data is in the url (GET) or the req body (POST)
  util.log('Request recieved: \nmethod: ' + req.method + '\nurl: ' + req.url) // this line logs just the method and url
  if(req.method==='OPTIONS'){
    res.writeHead(200);
    res.end();
  }else if(req.method==='POST'){
    var body = [];
    //h katallhlh kefalida
    res.writeHead(200, {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
    });
    //diavase data
    req.on("data", (chunk) => {
      console.log(chunk);
      body.push(chunk);
    });
    //otan exeis diavasei olo to data
    req.on("end", () => {
      var mdata = Buffer.concat(body).toString();
      mdata = JSON.parse(mdata);//parsing json
      console.log(mdata);
      pool.getConnection(async function(err, con) {
        console.log("Connected");
        const query = util.promisify(con.query).bind(con);//g9ia na exw promises
        var mquery = "DELETE FROM service WHERE id="
                      +mdata.service.id+";";
        query(mquery);
        mquery = "DELETE FROM employee_service WHERE sid="
                 +mdata.service.id+";";
        query(mquery);
        if (err){
          console.log(err);
        }
        message = {state : 0, msg : "Employee has been deleted succesfully."};
        res.write(JSON.stringify(message));
        res.end();
        con.release();
      });//telos connect
    });//req on end

    res.on('error', (err) => {
      console.error(err);
    });
  }//end if
});
// gia delete service
// retrieve services
app.all('/retrieveServices',function (req, res) {
  console.log('Request received: ');
  util.inspect(req) // this line helps you inspect the request so you can see whether the data is in the url (GET) or the req body (POST)
  util.log('Request recieved: \nmethod: ' + req.method + '\nurl: ' + req.url) // this line logs just the method and url
  if(req.method==='OPTIONS'){
    res.writeHead(200);
    res.end();
  }else if(req.method==='POST'){
    var body = [];
    //h katallhlh kefalida
    res.writeHead(200, {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
    });
    //diavase data
    req.on("data", (chunk) => {
      console.log(chunk);
      body.push(chunk);
    });
    //otan exeis diavasei olo to data
    req.on("end", () => {
      var mdata = Buffer.concat(body).toString();
      mdata = JSON.parse(mdata);//parsing json
      console.log(mdata);
      pool.getConnection(async function(err, con) {
        console.log("Connected");
        const query = util.promisify(con.query).bind(con);//g9ia na exw promises
        var mquery = "SELECT * FROM service WHERE shop_name like \'"
                      +mdata.employee.shop_name+"\' ;";
        var result = await query(mquery);
        if (err){
          console.log(err);
        }
        if(result.length == 0){// Store Service
          message = {state : 0, msg : "There are no services stored yet. \nTry creating one ."};
          res.write(JSON.stringify(message));
          res.end();
        }else{
          message = {state : 1,services : result ,msg : "Services retrieved succesfully."};
          res.write(JSON.stringify(message));
          res.end();
        }
        con.release();
      });//telos connect
    });//req on end

    res.on('error', (err) => {
      console.error(err);
    });
  }//end if
});
// gia retrieve services
// update services
app.all('/updateService',function (req, res) {
  console.log('Request received: ');
  util.inspect(req) // this line helps you inspect the request so you can see whether the data is in the url (GET) or the req body (POST)
  util.log('Request recieved: \nmethod: ' + req.method + '\nurl: ' + req.url) // this line logs just the method and url
  if(req.method==='OPTIONS'){
    res.writeHead(200);
    res.end();
  }else if(req.method==='POST'){
    var body = [];
    //h katallhlh kefalida
    res.writeHead(200, {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
    });
    //diavase data
    req.on("data", (chunk) => {
      console.log(chunk);
      body.push(chunk);
    });
    //otan exeis diavasei olo to data
    req.on("end", () => {
      var mdata = Buffer.concat(body).toString();
      mdata = JSON.parse(mdata);//parsing json
      console.log(mdata);
      pool.getConnection(async function(err, con) {
        console.log("Connected");
        const query = util.promisify(con.query).bind(con);//g9ia na exw promises
        var mquery = "UPDATE service SET name=\'"+mdata.service.name
                     +"\',cost="+mdata.service.cost
                     +",duration="+mdata.service.duration
                     +" WHERE id = "+mdata.service.id+" ;";
        var result = await query(mquery);
        if (err){
          console.log(err);
        }
        message = {state : 0, msg : "Service is updated."};
        res.write(JSON.stringify(message));
        res.end();
        con.release();
      });//telos connect
    });//req on end

    res.on('error', (err) => {
      console.error(err);
    });
  }//end if
});
// gia update services
// save service
app.all('/saveService',function (req, res) {
  console.log('Request received: ');
  util.inspect(req) // this line helps you inspect the request so you can see whether the data is in the url (GET) or the req body (POST)
  util.log('Request recieved: \nmethod: ' + req.method + '\nurl: ' + req.url) // this line logs just the method and url
  if(req.method==='OPTIONS'){
    res.writeHead(200);
    res.end();
  }else if(req.method==='POST'){
    var body = [];
    //h katallhlh kefalida
    res.writeHead(200, {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
    });
    //diavase data
    req.on("data", (chunk) => {
      console.log(chunk);
      body.push(chunk);
    });
    //otan exeis diavasei olo to data
    req.on("end", () => {
      var mdata = Buffer.concat(body).toString();
      mdata = JSON.parse(mdata);//parsing json
      console.log(mdata);
      pool.getConnection(async function(err, con) {
        console.log("Connected");
        const query = util.promisify(con.query).bind(con);//g9ia na exw promises
        var mquery = "SELECT * FROM service WHERE name like \'"
                      +mdata.service.name+"\' and shop_name like \'"
                      +mdata.employee.shop_name+"\' ;";
        var result = await query(mquery);
        if (err){
          console.log(err);
        }
        if(result.length == 0){// Store Service
          var mquery = "INSERT INTO service(name, duration , cost , shop_name) values (\'"
                        +mdata.service.name+"\' ,"+mdata.service.duration+","
                        +mdata.service.cost+",\'"+mdata.employee.shop_name+"\');";
          var result = await query(mquery);
          message = {state : 1,msg : "Service stored succesfully."};
          res.write(JSON.stringify(message));
          res.end();
        }else{
          message = {state : 0, msg : "Service exists ."};
          res.write(JSON.stringify(message));
          res.end();
        }
        con.release();
      });//telos connect
    });//req on end

    res.on('error', (err) => {
      console.error(err);
    });
  }//end if
});
// gia service save
// gia login
app.all('/login',function (req, res) {
  console.log('Request received: ');
  util.inspect(req) // this line helps you inspect the request so you can see whether the data is in the url (GET) or the req body (POST)
  util.log('Request recieved: \nmethod: ' + req.method + '\nurl: ' + req.url) // this line logs just the method and url
  if(req.method==='OPTIONS'){
    res.writeHead(200);
    res.end();
  }else if(req.method==='POST'){
    var body = [];
    //h katallhlh kefalida
    res.writeHead(200, {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
    });
    //diavase data
    req.on("data", (chunk) => {
      console.log(chunk);
      body.push(chunk);
    });
    //otan exeis diavasei olo to data
    req.on("end", () => {
      var mdata = Buffer.concat(body).toString();
      mdata = JSON.parse(mdata);//parsing json

      pool.getConnection(async function(err, con) {
        console.log("Connected");
        const query = util.promisify(con.query).bind(con);//g9ia na exw promises
        var mquery = "SELECT * FROM user WHERE email like \'"+mdata.username+"\';";
        var result = await query(mquery);
        if (err){
          console.log(err);
        }
        if(result.length == 0){
          message = {state : 0,msg :"Invalid e-mail or password."};
          res.write(JSON.stringify(message));
          res.end();
        }else{
          var user = result[0];
          if(user.type_of_user==='employee'){//if user is employee
            mquery = "SELECT * FROM employee WHERE id = "+user.id+";";
            result = await query(mquery);
            message = {state : 1,msg : "Connection Granted",user : user,employee: result[0],client : null};
            res.write(JSON.stringify(message));
            res.end();
          }
        }
        con.release();
      });//telos connect
    });//req on end

    res.on('error', (err) => {
      console.error(err);
    });
  }//end if
});
//gia login
// gia sign up
app.all('/signUp',function (req, res) {
  console.log('Request received: ');
  util.inspect(req) // this line helps you inspect the request so you can see whether the data is in the url (GET) or the req body (POST)
  util.log('Request recieved: \nmethod: ' + req.method + '\nurl: ' + req.url) // this line logs just the method and url
  if(req.method==='OPTIONS'){
    res.writeHead(200);
    res.end();
  }else if(req.method==='POST'){
    var body = [];
    //h katallhlh kefalida
    res.writeHead(200, {
      'Content-Type': 'text/plain',
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE'
    });
    //diavase data
    req.on("data", (chunk) => {
      console.log(chunk);
      body.push(chunk);
    });
    //otan exeis diavasei olo to data
    req.on("end", () => {
      var mdata = Buffer.concat(body).toString();
      mdata = JSON.parse(mdata);//parsing json
      console.log(mdata);
      pool.getConnection(async function(err,con) {
        console.log("Connected");
        const query = util.promisify(con.query).bind(con);//g9ia na exw promises
        var mquery = "SELECT * FROM user WHERE email like \'"+mdata.email+"\';";
        var result = await query(mquery);
        console.log(result)
        if (err){
          console.log(err);
          message = {state : 0,msg :"Network Error"};
          res.write(JSON.stringify(message));
          res.end();
          throw err
        }
        if(result.length > 0){
          console.log("exists");
          message = {state : 0,msg :"User with this email address exists."};
        }else{
          var mquery = "insert into user(name,surname,password,email,phone_num,type_of_user)"+
                       "values(\'"+mdata.name+"\',\'"+mdata.surname+"\',\'"+mdata.password+"\',\'"+mdata.email
                       +"\',\'"+mdata.phone_num+"\',\'"+mdata.type_of_user+"\');";
          query(mquery);
          message = {state : 1,msg : "Connection Granted"};
        }
        await res.write(JSON.stringify(message));
        await res.end();

        con.release();
      });//telos connect
    });//req on end

    res.on('error', (err) => {
      console.error(err);
    });
  }//end if
});
//gia sign up


app.listen(8080, function() {
console.log('Node app is running on port 8080');
});
module.exports = app;
