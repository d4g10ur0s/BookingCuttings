import React, { useState, useEffect } from 'react';
import type {PropsWithChildren} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {EmployeeContainer, ShopSection} from "./Shop.tsx"
import TheDay from "./ShowDay.tsx"

function DayCell(props) : JSX.Element{

  const [day , setDay] = useState(props.day)
  const [text , setText] = useState("");

  const dayPicked = () => {
    props.dayPicked(day);
  }

  useEffect( () => {
    if(day == null){setText("");}
    else{setText(props.day.dat);}
  } ,[day]);

  return(
    <TouchableOpacity
      style={styles.dayCell}
      onPress={dayPicked}
    >
      <Text>
        {text}
      </Text>
    </TouchableOpacity>
  )
}

function DayName(props){
  return(
    <Text
      style={styles.dayName}
    >
      {props.dname}
    </Text>
  )
}

function CustomCalendar(props){

  const [month , setMonth] = useState(new Date().toLocaleString('default', { month: 'long' }));
  const [days , setDays] = useState([]);
  const [daycells , setDayCells] = useState([]);

  const toDay = (day) => {
    console.log(props);
    props.toDay(day);
  }

  useEffect(() => {
    if(days.length==0){
      var daynames = [];
      var arr = ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"];
      for( i in arr){
        daynames.push(<DayName dname={arr[i]} />)
      }
      var date = new Date();
      var firstDay = new Date(date.getFullYear(), date.getMonth(), 2).toLocaleString('default', { weekday: 'short' });
      var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0).toLocaleString('default', { weekday: 'short' });
      firstDay=firstDay.split(',');
      lastDay=lastDay.split(',');
      var ts = 0;
      var counter = 0;
      var te = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();
      var tcontent = [];
      var f = true;
      while( counter < 35 ){
        var mday = {
          dat : 0,
          month : date.getMonth(),
          year : date.getFullYear(),
        };
        if(counter <= 7 && f){
          if(arr[counter] == firstDay[0]){
            mday.dat = 1;
            tcontent.push(<DayCell day={mday} dayPicked={toDay}/>)
            ts = counter;
            f = false;
          }else{
            //prepei na pros8esw kena
            tcontent.push(<DayCell day={null} dayPicked={toDay} kati={"kati"}/>)
          }
        }else if (counter-ts+1 <= te){
          //edw akoma vazw meres
          mday.dat = counter-ts+1;
          tcontent.push(<DayCell day={mday} dayPicked={toDay} kati={"kati"}/>)
        }else{
          //prepei na pros8esw kena
          tcontent.push(<DayCell day={null} dayPicked={toDay} kati={"kati"}/>)
        }
        counter+=1;
      }
      setDays(daynames.slice());
      setDayCells(tcontent);
    }//else{setDays(null);}
  } , [month]);

  return (
    <View
      style={styles.calendarView}
    >
      <Text
        style={styles.calendarHeader}
      >
        {month}
      </Text>
      <View
        style={styles.daysView}
      >
        {days}
      </View>
      <View
        style={{"flex":1,"flexDirection" : "row","alignSelf":"center","padding" : 5, "marginLeft" : 5,"marginRight" : 5,}}
      >
      {daycells.slice(0,7)}
      </View>
      <View
        style={{"flex":2,"flexDirection" : "row","alignSelf":"center","padding" : 5, "marginLeft" : 5,"marginRight" : 5,}}
      >
      {daycells.slice(7,14)}
      </View>
      <View
        style={{"flex":3,"flexDirection" : "row","alignSelf":"center","padding" : 5, "marginLeft" : 5,"marginRight" : 5,}}
      >
      {daycells.slice(14,21)}
      </View>
      <View
        style={{"flex":4,"flexDirection" : "row","alignSelf":"center","padding" : 5, "marginLeft" : 5,"marginRight" : 5,}}
      >
      {daycells.slice(21,28)}
      </View>
      <View
        style={{"flex":5,"flexDirection" : "row","alignSelf":"center","padding" : 5,}}
      >
      {daycells.slice(28,35)}
      </View>
    </View>
  );

}

function UserApp(props): JSX.Element {

  const [content , setContent] = useState(null);
  const [employee , setEmployee] = useState(props.employee)

  const toDay = (day) => {
    setContent(<TheDay day={day} />)
  }
  // retrieve employees from database
  const toShop = async () => {
    setContent(<ShopSection employee={employee} />);
  }

  function toCalendar(){
    setContent(<CustomCalendar toDay={toDay}/>);
  }

  useEffect(() => {
    if(content == null){setContent(<CustomCalendar toDay={toDay}/>);}
  }, [content]);

  return (
    <View
    >
      <View
        style = {styles.buttonView}
      >
        <TouchableOpacity
          style={styles.controlButtons}
          onPress={() => toCalendar()}
        >
          <Text>
            {"Calendar"}
            </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.controlButtons}
          onPress={() => toShop()}
        >
          <Text>
            {"Shop"}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.controlButtons}
        >
          <Text>
            {"Options"}
          </Text>
        </TouchableOpacity>
      </View>
      {content}
    </View>
    );
}

const styles = StyleSheet.create({
  calendarView : {
    backgroundColor : '#322c47c5',
    borderRadius : 5,
    padding : 15,
    margin : 10,
  },
  calendarHeader : {
    alignSelf : "center",
    margin : 5,
    fontSize : 35,
    textAlign : "center",
    color : "#FFFFFFFF",
    fontWeight : 700,
  },
  daysView : {
    alignSelf : "center",
    width : "110%",
    flexDirection : 'row',
    borderRadius : 8,
    padding : 5,
    marginLeft : 5,
    marginRight : 5,
    marginTop : 5,
    marginBottom : 10,
    backgroundColor : "#FFFFFF59",
    color : "#000000FF",
  },
  dayName : {
    backgroundColor : "#ebd2817b",
    fontSize : 14,
    margin : 2,
    paddingTop : 3,
    paddingBottom : 3,
    paddingRight : 10,
    paddingLeft : 10,
    borderRadius : 8,
  },
  dayCell : {
    width : 43,
    backgroundColor : "#ebd2817b",
    fontSize : 15,
    margin : 2,
    padding : 10,
    borderRadius : 8,
  },
  buttonView : {
    width : "95%",
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection : "row",
    backgroundColor : "#322c47c5",
    paddingLeft : 7,
    paddingRight : 7,
    paddingTop : 2,
    paddingBottom : 2,
    marginTop : 10,
    marginBottom : 2,
    alignSelf : "center",
    borderRadius : 8,
  },
  controlButtons : {
    fontSize : 12,
    textAlign : "center",
    margin : 5,
    padding : 8,
    borderRadius : 8,
    backgroundColor : "#ebd2817b",
  },
});

export default UserApp;
