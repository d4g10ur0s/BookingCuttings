/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React, { useState, useEffect } from 'react';
import type {PropsWithChildren} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import UserApp from "./UserApp.tsx"

export function ErrorInput(props){
  return (
    <Text
      style={styles.usernameError}
    >
      {props.msg}
    </Text>
  );
}

function LogInForm(props) {
  // the user
  const [user, setUser] = useState({
                                  name : "",
                                  surname : "" ,
                                  email : "" ,
                                  phone_num : "" ,
                                  password : "" ,
                                  type_of_user : "" ,
                                  });
  // form state , if 1 then is log in else is sign up
  const [formState ,setFormState] = useState(1);
  const [typeOfForm ,setTypeOfForm] = useState("Log In");
  // user log in info
  const [username, setUsername] = useState("aledadu@hotmail.com");
  const [password, setPassword] = useState("Den8aKsexasw");
  const [passwordError , setPasswordError] = useState(null);
  // user login inputs
  const [usernameInput , setUsernameInput] = useState(<TextInput style={styles.logInput}
                                                                 placeholder={"Username"}
                                                                 onChangeText={(text) => setUsername(text)}/>);
  const [passwordInput , setPasswordInput] = useState(<TextInput style={styles.logInput}
                                                                 placeholder={"Password"}
                                                                 onChangeText={(text) => setPassword(text)}/>);
  // user sign in info
  const [name ,setName] = useState("");
  const [nameError , setNameError] = useState(null);
  const [surname ,setSurname] = useState("");
  const [surnameError , setSurnameError] = useState(null);
  const [pass2 ,setPass2] = useState("");
  const [pass2Error , setPass2Error] = useState(null);
  const [email ,setEmail] = useState("");
  const [emailError , setEmailError] = useState(null);
  const [phone ,setPhone] = useState("");
  const [phoneError , setPhoneError] = useState(null);
  // user sign in inputs
  const [nameInput , setNameInput] = useState(null);
  const [surnameInput , setSurnameInput] = useState(null);
  const [emailInput , setEmailInput] = useState(null);
  const [phoneInput , setPhoneInput] = useState(null);
  // user log in validation error
  const [connectionError, setConnectionError] = useState("");
  // user sign in username error
  const [usernameError, setUsernameError] = useState();
  // sign up name check callback
  const nameCheck = (smallname) => {
    if(smallname.length>3){
      var tuser = user;
      tuser.name = smallname.replace(/\s/g, "");
      setUser(tuser);
      setNameError(null);
    }else{
      setNameError(<ErrorInput msg = {"Name must be over 3 characters long."} />);
    }
  }
  // sign up surname check callback
  const surnameCheck = (surname) => {
    if(surname.length>3){
      setSurnameError(null);
      var tuser = user;
      tuser.surname = surname.replace(/\s/g, "");
      setUser(tuser);
    }else{
      setSurnameError(<ErrorInput msg = {"Surname must be over 3 characters long."} />);
    }
  }
  // sign up email check
  const emailCheck = (email) => {
    const emailRegex: RegExp = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
    if (emailRegex.test(email)) {
      setEmailError(null);
      var tuser = user;
      tuser.email = email.replace(/\s/g, "");
      setUser(tuser);
    }else{setEmailError(<ErrorInput msg = {"You provided an invalid e-mail address."} />);}
  }
  // sign up password callback
  const passwordCheck = (password) => {
    const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$/;
    if( passwordRegex.test(password)){//prepei na ftiaksw ke to regex
      setPasswordError(null);
      var tuser = user;
      tuser.password = password;
      setUser(tuser);
    }else{
      setPasswordError(<ErrorInput msg = {"Password must : \n At least 8 characters long \n Contains at least one digit (0-9) \n Contains at least one lowercase letter (a-z) \n Contains at least one uppercase letter (A-Z) \n Contains at least one special character (!@#$%^&*)"} />);
    }
    setPassword(password);
  };
  // sign up pass2 callback
  const pass2Check = (text) => {
    if(text === user.password){
      setPass2Error(null);
    }else{setPass2Error(<ErrorInput msg = {"Passwords must match."} />);}
  }
  // sign up phone callback
  const phoneCheck = (phone) => {
    const phoneRegex = /^\d{10}$/;
    if(phoneRegex.test(phone)){
      setPhoneError(null);
      var tuser = user;
      tuser.phone_num = phone.replace(/\s/g, "");
      setUser(tuser);
    }else{setPhoneError(<ErrorInput msg = {"Phone must be 10 digits long."} />);}
  }
  // sign up to application
  const signUp = () => {
    if(formState==1){
      setUsernameInput(null);
      setPasswordInput(<TextInput style={styles.logInput} placeholder={"Password"} onChangeText={(text) => passwordCheck(text)}/>);
      setPass2(<TextInput style={styles.logInput} placeholder={"Repeat Password"} onChangeText={(text) => pass2Check(text)}/>);
      setNameInput(<TextInput style={styles.logInput} placeholder={"Name"} onChangeText={(text) => nameCheck(text)}/>);
      setSurnameInput(<TextInput style={styles.logInput} placeholder={"Surname"} onChangeText={(text) => surnameCheck(text)}/>);
      setEmailInput(<TextInput style={styles.logInput} placeholder={"Email"} onChangeText={(text) => emailCheck(text)}/>);
      setPhoneInput(<TextInput style={styles.logInput} placeholder={"Phone Num."} onChangeText={(text) => phoneCheck(text)}/>);
      setTypeOfForm("Sign Up")
      setFormState(0);
    }else{
      if(emailError == null && nameError == null && surnameError == null && phoneError == null && passwordError==null){
        var tuser = user;
        tuser.type_of_user = "client";
        console.log(user);
        fetch('http://192.168.1.226:8080/signUp', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(user)
        })
        .then(response => {
          if (response.ok) {
            return response.json();
          } else {
            throw new Error('Network response was not ok');
          }
        })
        .then(data => {
          if(data.state==0){
            //yparxei error , 8a to grapseis katw katw
            setConnectionError(<ErrorInput msg = {data.msg} />);
          }
        })
        .catch(error => {
          console.error('There was a problem with the fetch operation:', error);
        });
      }else{console.log("Not valid information");}
    }
  }

  const logIn = () => {
    //192.168.1.226
    //176.58.139.29
    if(formState==1){
      fetch('http://192.168.1.226:8080/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username: username,
          password: password,
        })
      })
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error('Network response was not ok');
        }
      })
      .then(data => {
        if(data.state==0){
          //yparxei error , 8a to grapseis katw katw
          setConnectionError(<ErrorInput msg = {data.msg} />);
        }else{
          if(data.client == null){
            props.logCallback({user : data.user, employee : data.employee});
          }else{
            props.logCallback({user : data.user, client : data.client});
          }
        }
      })
      .catch(error => {
        console.error('There was a problem with the fetch operation:', error);
      });
    }else{
      setUsernameInput(<TextInput style={styles.logInput}
                                  placeholder={"Username"}
                                  onChangeText={(text) => setUsername(text)}/>);
      setPasswordInput(<TextInput style={styles.logInput}
                                  placeholder={"Password"}
                                  onChangeText={(text) => setPassword(text)}/>);
      setPass2(null);
      setNameInput(null);
      setSurnameInput(null);
      setEmailInput(null);
      setPhoneInput(null);
      setTypeOfForm("Log In")
      setFormState(1);
    }
  }

  const usernameCheck = (username) => {
    if( username.length < 5){setUsernameError(<ErrorInput msg = {"Username must be over 5 characters long."} />);}
    else{setUsernameError(null);}
    setUsername(username);
  };

  return (
    <View
     style={styles.logView}
    >
      <View>
        <Text
         style={styles.welcomeHeader}
        >
          {"Welcome"}
        </Text>
      </View>
      <View
       style={styles.logForm}
      >
        <Text
         style={styles.welcomeHeader}
        >
          {typeOfForm}
        </Text>
        {usernameInput}
        {usernameError}
        {nameInput}
        {nameError}
        {surnameInput}
        {surnameError}
        {emailInput}
        {emailError}
        {phoneInput}
        {phoneError}
        {passwordInput}
        {passwordError}
        {pass2}
        {pass2Error}
        <View
        style={{"alignItems" : "center"}}
        >
          {connectionError}
        </View>
        <View
        style={styles.buttonView}
        >
          <TouchableOpacity
           style = {styles.logButton}
           onPress = {() => logIn()}
          >
            <Text>
              {"Log In"}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
           style = {styles.signButton}
           onPress = {() => signUp()}
          >
            <Text>
              {"Sign Up"}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );

}

function App(): JSX.Element {

  function logIn(user){
    console.log(user)
    setContent(<UserApp employee={user}/>);
  }
  const [content , setContent] = useState(<LogInForm logCallback={logIn}/>);

  return (
    <ScrollView
    contentInsetAdjustmentBehavior="automatic"
    >
      {content}
    </ScrollView>
    );
}

const styles = StyleSheet.create({
  logView : {
    flex : 1,
    alignSelf : "center",
    padding : 5,
  },
  welcomeHeader : {
    textAlign : "center",
    fontSize : 35,
    fontWeight : 700,
    margin : 15,
    color : "#FFFFFFFF",
  },
  logForm : {
    width : 300,
    backgroundColor : '#ff4d4dad',
    borderRadius : 5,
    padding : 15,
  },
  logInput : {
    borderRadius : 8,
    padding : 10 ,
    fontSize : 15,
    marginLeft : 15,
    marginRight : 15,
    marginTop : 5,
    marginBottom : 10,
    backgroundColor : "#FFFFFF59",
    color : "#000000FF",
  },
  logButton : {
    alignSelf : "center",
    backgroundColor : "#5544FFFF",
    margin : 5,
    paddingRight : 15,
    paddingLeft : 15,
    paddingTop : 5,
    paddingBottom : 5,
    borderRadius : 8,
  },
  signButton : {
    alignSelf : "center",
    backgroundColor : "#e65c00FF",
    margin : 5,
    paddingRight : 15,
    paddingLeft : 15,
    paddingTop : 5,
    paddingBottom : 5,
    borderRadius : 8,
  },
  buttonView : {
    margin : 15,
    alignSelf : "center",
    flex : 1,
    flexDirection : "row",
  },
  usernameError : {
    fontSize : 10,
    textAlign : "left",
    marginTop : 1,
    marginBottom : 5,
    paddingLeft : 25,
    color : "#FF5555FF",
  },
});

export default App;
