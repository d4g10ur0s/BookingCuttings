import React, { useState, useEffect } from 'react';
import type {PropsWithChildren} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  TextInput,
  TouchableOpacity,
  View,
  Image,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

export function EmployeeCell(props) : JSX.Element {
  const [selected, setSelected] = useState(false);
  const [employee , setEmployee] = useState({num : props.num, employee : props.employee,});

  const employeeSelected = () => {
    props.handleSelection(!selected, employee)
    setSelected(!selected);
  }

  return (
    <View
      style={styles.employeeCell}
    >
      <Image
       source={{uri: 'https://reactjs.org/logo-og.png'}}
       style={{alignSelf : "flex-start",width: 50, height: 50,borderRadius : 8,marginBottom : 5,marginTop : 5,}}
      />
      <Text
        style={{"alignSelf" : "center","margin" : 5,}}
      >
        {employee.employee.name + " " + employee.employee.surname}
      </Text>
      <TouchableOpacity
        style={selected ? styles.selectedButton : styles.selectButton}
        onPress={employeeSelected}
      >
        <Text>
          {"Select"}
        </Text>
      </TouchableOpacity>
    </View>
  );

}

export function EmployeeSelection(props) : JSX.Element {
  const [selections , setSelections] = useState([]);
  const [selected, setSelected] = useState({});
  // push employees to list
  const pushEmployee = (choice) => {
    var tSelected = selected ;
    tSelected[choice.num] = choice.employee;
    setSelected(tSelected);
  }
  // pop employees to list
  const popEmployee = (choice) => {
    var tSelected = selected ;
    delete tSelected[choice.num]
    setSelected(tSelected);
  }
  // select an employee
  const selectedEmployee = (state, employee) => {
    if(state){
      pushEmployee(employee);
    }else{
      popEmployee(employee);
    }
  }
  // retrieve employees from database
  const retrieveEmployees = async () =>{
    var empl = await fetch('http://192.168.1.226:8080/retrieveEmployees', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({user : props.user.user, employee: props.user.employee})
    })
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Network response was not ok');
      }
    })
    .then(data => {
      if(data.state==0){
        //yparxei error , 8a to grapseis katw katw
        setConnectionError(<ErrorInput msg = {data.msg} />);
      }else{
        var empl = []
        // services are retrieved
        //create UI
        for(i in data.employees){
          empl.push(data.employees[i]);
        }
        return empl
      }
    })
    .catch(error => {
      console.error('There was a problem with the fetch operation:', error);
    });
    return empl;
  }
  // create employee cells
  const createEmployeeCells = async (employees) => {
    //1. Create UI for each service
    var ret = [];
    for(i in employees){
      ret.push(<EmployeeCell num={i} user={props.employee} employee={employees[i]} handleSelection={selectedEmployee}/>)
    }
    return ret;
  }
  // create employee cells
  const onLoad = async () => {
    // get employee's
    var employees = await retrieveEmployees();
    setSelections(await createEmployeeCells(employees));
  }
  // retrieve employees from database
  useEffect(() => {// auto eilikrina den kserw giati uparxei
    if(selections.length==0){
      console.log("edw eisai")
      onLoad();
    }else{
      console.log("gemise");
    }
  }, [selections]);
  // UI
  return (
    <ScrollView
      style={styles.employeeSelection}
    >
      {selections.slice()}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  employeeSelection : {
    backgroundColor : '#0000005C',
    alignSelf : "center",
    margin : 5,
    height : 150,
    borderRadius : 8,
  },
  employeeCell : {
    width : "100%",
    flexDirection: "row",
    margin : 2,
    alignSelf : "center",
    borderRadius : 8,
    borderTopWidth : 1,
    borderBottomWidth : 2,
    padding : 2,
  },
  selectButton : {
    alignSelf : "center",
    backgroundColor : "#ebd2817b",
    paddingLeft : 10,
    paddingRight : 10,
    paddingTop : 5,
    paddingBottom : 5,
    borderRadius : 8,
  },
  selectedButton : {
    alignSelf : "center",
    backgroundColor : "#ebd2814b",
    paddingLeft : 10,
    paddingRight : 10,
    paddingTop : 5,
    paddingBottom : 5,
    borderRadius : 8,
  }
});
