import React, { useState, useEffect } from 'react';
import type {PropsWithChildren} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  TextInput,
  TouchableOpacity,
  View,
  Image,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {ErrorInput} from "./App.tsx"
import {DeletionModal, ErrorModal , PickerModal} from "./AppPopUps.tsx"

export function ServiceContainer(props){
  const [pickerVisible ,setPickerVisible] = useState(false);
  const [editable , setEditable] = useState(false);
  const [submit, setSubmit] = useState(null);
  const [service, setService] = useState(null);
  const [name, setName] = useState(props.service.name);
  const [duration, setDuration] = useState("Duration : "+props.service.duration+" min");
  const [cost, setCost] = useState("Cost : "+props.service.cost+" eur");
  const [assigned, setAssigned] = useState("Assigned : 10 pers.");
  // service name error
  const [nameError , setNameError] = useState(null);
  // service name check
  const serviceNameCheck = (name) => {
    if(name.length>3){
      setName(name);
      setNameError(null);
    }else{
      setNameError(<ErrorInput msg = {"Name must be over 3 characters long."} />);
    }
  }
  // service cost error
  const [costError ,setCostError] = useState(null);
  // service cost check
  const serviceCostCheck = (cost) => {
    if(isNaN(cost)){
      setCostError(<ErrorInput msg = {"Cost must be a valid number."} />);
    }else{
      if(cost > 200 || cost < 5){
        setCostError(<ErrorInput msg = {"Cost must be between 5 and 200 eur."} />);
      }else{
        setCost(cost);
        setCostError(null);
      }
    }
  }
  // service duration error
  const [durationError , setDurationError] = useState(null);
  // service duration check
  const serviceDurationCheck = (duration) => {
    if(isNaN(duration)){
      setDurationError(<ErrorInput msg = {"Duration must be a valid number."} />);
    }else{
      if(duration > 240 || duration < 5){
        setDurationError(<ErrorInput msg = {"Duration must be between 5 and 240 minutes."} />);
      }else{
        setDuration(duration);
        setDurationError(null);
      }
    }
  }
  const toModification = () => {
    if(!editable){
      setService({
        name : name,
        duration : duration,
        cost : cost,
      })
      setSubmit(<TouchableOpacity style={styles.submitButton}><Text style={{alignSelf:"center"}}>{"Submit"}</Text></TouchableOpacity>)
      console.log(props.service.duration);
      setDuration(props.service.duration);
      setCost(props.service.cost);
    }else{
      setDuration(service.duration);
      setCost(service.cost);
      setName(service.name);
      setDurationError(null);
      setCostError(null);
      setNameError(null);
      setSubmit(null)
    }
    setEditable(!editable);
  }
  /*
    4 modal
  */
  // show pick
  function showPick() {
    setPickerVisible(true)
  }
  // cancel pick
  const cancelPick = () => {
    setPickerVisible(false);
  }
  // submit pick
  const submitPick = () => {
    setPickerVisible(false);
  }
  return(
    <View
      style={styles.serviceContainer}
    >
      <View
        style={styles.theService}
      >
        <View
          style={styles.dayCell}
        >
          <TextInput
            style={editable ? styles.serviceNameEnabled:styles.serviceName}
            editable={editable}
            onChangeText = {(text) => serviceNameCheck(text)}
          >
            {name}
          </TextInput>
          {nameError}
          <View
            style={styles.contentView}
          >
            <TextInput
              style={editable ? styles.serviceContentEnabled : styles.serviceContent}
              editable={editable}
              onChangeText={(text) => serviceDurationCheck(text)}
            >
              {duration}
            </TextInput>
            {durationError}
            <TextInput
              style={editable ? styles.serviceContentEnabled : styles.serviceContent}
              editable={editable}
              onChangeText={(text) => serviceCostCheck(text)}
            >
              {cost}
            </TextInput>
            {costError}
            <Text
              style={styles.serviceContent}
            >
              {assigned}
            </Text>
            {submit}
          </View>
        </View>
      </View>
      <ScrollView
        horizontal={true}
        style = {styles.buttonScrollView}
      >
        <TouchableOpacity
          style={styles.controlButtons}
          onPress={() => toModification()}
        >
          <Text>
            {"Modify Service"}
            </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.controlButtons}
          onPress={() => showPick()}
        >
          <Text>
            {"Assign Service"}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.controlButtons}
          onPress={() => toCalendar()}
        >
          <Text>
            {"Service Statistics"}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            "fontSize" : 12,
            "textAlign" : "center",
            "marginLeft" : 8,
            "marginTop" : 2,
            "marginBottom" : 2,
            "marginRight" : 8,
            "padding" : 8,
            "borderRadius" : 8,
            "backgroundColor" : "#FF4455FF",
          }}
        >
          <Text>
            {"Delete Service"}
          </Text>
        </TouchableOpacity>
      </ScrollView>
      <PickerModal user={props.user} visible={pickerVisible} isVisible={cancelPick} submitPick={submitPick} serviceName={name}/>
    </View>
  )
}

export function ServiceSection(props){

  const [content , setContent] = useState(props.services);

  return (
    <View
      style={styles.employeeSectionView}
    >
      {content}
    </View>
  );

}

const styles = StyleSheet.create({
  shopView : {
    backgroundColor : '#ff4d4dad',
    borderRadius : 5,
    padding : 15,
    margin : 10,
  },
  serviceName : {
    alignSelf : "center",
    margin : 5,
    fontSize : 22,
    textAlign : "center",
    color : "#000000FF",
    fontWeight : "700",
    borderRadius : 8,
    borderBottomWidth : 1,
    paddingLeft : 50 ,
    paddingRight : 50 ,
  },
  serviceNameEnabled : {
    alignSelf : "center",
    margin : 5,
    fontSize : 22,
    textAlign : "center",
    color : "#000000FF",
    fontWeight : "700",
    borderRadius : 8,
    borderBottomWidth : 1,
    paddingLeft : 50 ,
    paddingRight : 50 ,
    backgroundColor : "#FFFFFF59",
  },
  serviceContent : {
    alignSelf : "center",
    margin : 5,
    fontSize : 17,
    textAlign : "center",
    color : "#000000FF",
    fontWeight : "400",
    padding : 5,
  },
  serviceContentEnabled : {
    width : 190,
    alignSelf : "center",
    margin : 5,
    fontSize : 17,
    textAlign : "center",
    color : "#000000FF",
    fontWeight : "400",
    backgroundColor : "#FFFFFF59",
    borderRadius : 8,
  },
  serviceContainer : {
    flexDirection : "column",
    borderRadius : 8,
    backgroundColor : "#FFFFFF59",
    marginLeft : 5,
    marginRight : 5,
    marginTop : 5,
    marginBottom : 10,
  },
  contentView : {
    alignSelf : "center",
    margin: 5,
  },
  buttonScrollView : {
    width : "100%",
    backgroundColor : "#F5E8BC35",
    paddingLeft : 7,
    paddingRight : 7,
    paddingTop : 2,
    paddingBottom : 2,
    marginTop : 10,
    marginBottom : 2,
    alignSelf : "center",
    borderRadius : 8,
  },
  controlButtons : {
    fontSize : 12,
    textAlign : "center",
    marginLeft : 8,
    marginTop : 2,
    marginBottom : 2,
    marginRight : 8,
    padding : 8,
    borderRadius : 8,
    backgroundColor : "#ebd2817b",
  },
  submitButton : {
    alignSelf : "center",
    alignItems : "center",
    justifyContent : "center",
    textAlign : "center",
    width : 100,
    fontSize : 10,
    marginLeft : 8,
    marginTop : 2,
    marginBottom : 2,
    marginRight : 8,
    padding : 8,
    borderRadius : 8,
    backgroundColor : "#554488FF",
  },
  daycell : {
    alignItems : "center",
    justifyContent : "center",
  },
});
