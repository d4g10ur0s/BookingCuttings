import React, { useState, useEffect } from 'react';
import type {PropsWithChildren} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  TextInput,
  TouchableOpacity,
  View,
  Image,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {EmployeeCell,EmployeeSelection} from "./AppSelections.tsx"

function AppointmentForm(props) : JSX.Element {
  const [selections , setSelections] = useState("");

  return (
    <View
      style={styles.appointmentForm}
    >
      <TextInput
        placeholder={"aaaa"}
      >
      </TextInput>
    </View>
  );

}

function NewAppointment(props) : JSX.Element {

  const [content , setContent] = useState(<EmployeeSelection />);

  return (
    <View
      style={styles.newAppointmentView}
    >
      {content}
    </View>
  );

}

function AppointmentSection(props) : JSX.Element {

  const [appointmentInfo , setAppointmentInfo] = useState("");

  return (
    <View
    style={styles.theAppointment}
    >
      <Text
        style={styles.appointmentBegginingTime}
      >
        {"Beggining Time : 12:40 p.m." }
      </Text>
    <ScrollView
      horizontal={true}
      style={styles.appointmentView}
    >
      <View
        style = {styles.clientInfo}
      >
        <Text
          style={styles.infoHeader}
        >
          {"Client Info"}
        </Text>
        <Text>{"Alexios Ntavlouros"}</Text>
      </View>
      <View
        style = {styles.employeeInfo}
      >
        <Text
          style={styles.infoHeader}
        >
          {"Employee Info"}
        </Text>
        <Text>{"Alexios Ntavlouros"}</Text>
      </View>
      <View
        style = {styles.serviceInfo}
      >
        <Text
          style={styles.serviceInfoHeader}
        >
          {"Service Info"}
        </Text>
        <Text>{"Duration : 35 min"}</Text>
        <Text>{"Cost : 10 eur"}</Text>
      </View>
    </ScrollView>
    <View
      style={styles.optionsView}
    >
      <TouchableOpacity
        style={styles.modifyButton}
      >
        <Text>
          {"Modify"}
          </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.deleteButton}
      >
        <Text>
          {"Delete"}
          </Text>
      </TouchableOpacity>
    </View>
    </View>
  );
}

function TheDay(props): JSX.Element {

  const [day , setDay] = useState(props.day);
  const [form, setForm] = useState(null);

  const [nap , setNap] = useState(true);
  const [taf , setTaf] = useState(true);

  const newAppointment = () => {
    setNap(!nap);
  }

  const toAppointmentForm = (employee) => {
    setNap(!nap);
    setTaf(!taf);
  }

  useEffect(() => {
    if(nap){
      setForm(null);
    }else{
      setForm(<EmployeeSelection selectedEmployee={toAppointmentForm}/>);
    }
  },[nap]);

  useEffect(() => {
    if(!taf){
      setForm(<AppointmentForm />);
    }
  },[taf]);

  return (
    <View
      style={styles.dayView}
    >
      <TouchableOpacity>
        <Text
          style={styles.modifyButton}
          onPress={newAppointment}
        >
          {"New Appointment"}
        </Text>
      </TouchableOpacity>
      {form}
      <Text
        style={styles.dayHeader}
      >
        {new Date(day.year, day.month, day.dat).toLocaleString('default', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' })}
      </Text>
      <View
        style={styles.appointmentSection}
      >
        <AppointmentSection />
      </View>
    </View>
    );
}

const styles = StyleSheet.create({
  dayView : {
    backgroundColor : '#322c47c5',
    borderRadius : 8,
    margin : 5,
    padding : 5,
  },
  dayHeader : {
    alignSelf : "center",
    fontSize : 22,
    fontWeight : "700",
    marginTop : 10,
  },
  theAppointment : {
    backgroundColor : '#0000005C',
    borderRadius : 8,
    margin : 5,
  },
  appointmentBegginingTime : {
    fontSize : 12,
    marginLeft : 10,
    marginTop : 5,
  },
  appointmentView : {
    margin : 5,
    flexDirection: "row",
  },
  infoHeader : {
    fontSize : 17,
    fontWeight : "700",
  },
  serviceInfoHeader : {
    fontSize : 17,
    fontWeight : "700",
  },
  clientInfo : {
    alignItems:"center",
    padding : 8,
  },
  employeeInfo : {
    alignItems:"center",
    padding : 8,
  },
  serviceInfo : {
    alignItems:"center",
    padding : 8,
  },
  optionsView : {
    flexDirection : "row",
    margin : 5,
    justifyContent : "space-between",
  },
  modifyButton : {
    backgroundColor : "#ebd2817b",
    paddingLeft : 10,
    paddingRight : 10,
    paddingTop : 5,
    paddingBottom : 5,
    borderRadius : 8,
    alignSelf : "flex-start",
  },
  deleteButton : {
    backgroundColor : "#FF4455FF",
    paddingLeft : 10,
    paddingRight : 10,
    paddingTop : 5,
    paddingBottom : 5,
    borderRadius : 8,
    alignSelf : "flex-end",
  },
  employeeSelection : {
    backgroundColor : '#0000005C',
    alignSelf : "center",
    paddingLeft : 20,
    paddingRight : 20,
    margin : 5,
    height : 150,
    borderRadius : 8,
  },
  employeeCell : {
    flexDirection: "row",
    margin : 1,
    alignSelf : "center",
    borderRadius : 8,
    borderTopWidth : 1,
    borderBottomWidth : 2,
    padding : 2,
  },
  selectButton : {
    alignSelf : "center",
    backgroundColor : "#ebd2817b",
    paddingLeft : 10,
    paddingRight : 10,
    paddingTop : 5,
    paddingBottom : 5,
    borderRadius : 8,
  },
  selectedButton : {
    alignSelf : "center",
    backgroundColor : "#ebd2814b",
    paddingLeft : 10,
    paddingRight : 10,
    paddingTop : 5,
    paddingBottom : 5,
    borderRadius : 8,
  }
});

export default TheDay;
